<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('index');

Route::get('/search', [MainController::class, 'search'])->name('search');

Route::get('/book_{id}', [MainController::class, 'detail'])->name('detail')->whereNumber('id');

Route::get('/user/mypage', function () {
    return view('motoyabooks.user.mypage');
})->middleware(['auth'])->name('mypage');

require __DIR__.'/auth.php';
