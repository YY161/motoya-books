@section('sidebar')
  <aside id="sidebar">
    <section class="publish-date">
      <div class="option">発売日</div>
      <ul>
        <li><a href="">近日30日以内</a></li>
        <li><a href="">近日7日以内</a></li>
        <li><a href="">本日</a></li>
        <li><a href="">過去7日以内</a></li>
        <li><a href="">過去30日以内</a></li>
      </ul>
    </section>
    <section class="junre">
      <div class="option">ジャンル</div>
      <ul>
        <li><a href="">ビジネス書</a></li>
        <li><a href="">小説</a></li>
        <li><a href="">漫画</a></li>
        <li><a href="">雑誌</a></li>
        <li><a href="">専門書</a></li>
      </ul>
    </section>
    <section class="price-range">
      <div class="option">価格帯</div>
      <ul>
        <li><a href="">0〜1,000円</a></li>
        <li><a href="">1,000〜2,000円</a></li>
        <li><a href="">2,000〜3,000円</a></li>
        <li><a href="">3,000〜5,000円</a></li>
        <li><a href="">5,000円〜</a></li>
      </ul>
    </section>
  </aside>
@endsection
