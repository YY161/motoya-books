@section('header')
  <header>
    <nav class="global-nav">
      <div class="nav-left">
        <div class="logo">MotoyaBooks</div>
      </div>
      <div class="nav-center">
        <form action="{{ route('search') }}" method="GET" class="search-form">
          <input type="search" value="" placeholder="検索" name="keyword" class="search-form__search-box">
          <button type="submit" class="search-form__search-button">
            <img src="{{ asset('img/icon-search.svg') }}" alt="" class="search-form__search-icon">
          </button>
        </form>
      </div>
      <div class="nav-right">
        <ul class="usermenu-list">
          <li class="usermenu-list__element">
            <a class="usermenu-list__link" href="">ログイン</a>
          </li>
          <li class="usermenu-list__element">
            <a class="usermenu-list__link" href="">会員登録</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
@endsection
