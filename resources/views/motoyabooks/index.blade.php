@extends('motoyabooks.components.common')

@section('id', 'index')

@include('motoyabooks.components.header', ['keyword' => $keyword ?? ''])

@section('content')
  <section class="popular-books">
    <h2>最近話題の本</h2>
    <ul class="book-list">
      @foreach ($books as $book)
      <li class="book-list__book">
        <a class="book-list__book_link" href="{{ route('detail', ['id' => $book->id]) }}">
          <img src="{{ asset('img/books/book_sasshi1.png') }}" alt="" class="book-list__book_feature">
          <p class="book-list__book_title">{{ $book->short_title }}</p>
          <div class="book-list__book_published_at">{{ $book->published_at }}</div>
          <span class="book-list__book_star-rating" data-rate="{{ $book->reviews_average }}"></span>
          <span class="book-list__book_reviews-count">({{ $book->reviews_count }})</span>
          <div class="book-list__book_price">¥ <span class="book-list__book_price-num">{{ $book->price }}</span></div>
        </a>
      </li>
      @endforeach
    </ul>
  </section>
@endsection
