@extends('motoyabooks.components.common')

@section('id', 'detail')

@include('motoyabooks.components.header', ['keyword' => $keyword ?? ''])

@section('content')
  <section class="book-detail">
    <div class="book-detail__left-area">
      <img src="{{ asset('img/books/book_sasshi1.png') }}" alt="" class="book-detail__feature">
    </div>
    <div class="book-detail__right-area">
      <div class="book-detail__title">{{ $book->title }}</div>
      <div class="book-detail__published-at">{{ $book->published_at }}</div>
      <span class="book-detail__star-rating" data-rate="{{ $book->reviews_average }}"></span>
      <span class="book-detail__reviews-count">（{{ $book->reviews_count }}件のレビュー）</span>
      <div class="book-detail__price">¥ <span class="book-detail__price-num">{{ $book->price }}</span></div>
      <p class="book-detail__description">{{ $book->description }}</p>
    </div>
  </section>
  <div class="horizontal-line"></div>
  <section class="book-review">
    <ul class="review-list">
      @foreach ($book->recent_reviews as $review)
        <li class="review-list__element">
          <div class="review-list__heading">
            <span class="review-list__star-rating" data-rate="{{ $review->score }}"></span>
            <span class="review-list__title">{{ $review->title }}</span>
          </div>
          <div class="review-list__posted_at">{{ $review->posted_at }}</div>
          <p class="review-list__detail">{{ $review->detail }}</p>
        </li>
      @endforeach

      @foreach ($book->other_reviews as $review)
        <li class="review-list__element">
          <div class="review-list__heading">
            <span class="review-list__star-rating" data-rate="{{ $review->score }}"></span>
            <span class="review-list__title">{{ $review->title }}</span>
          </div>
          <div class="review-list__posted_at">{{ $review->posted_at }}</div>
          <p class="review-list__detail">{{ $review->detail }}</p>
        </li>
      @endforeach
    </ul>
  </section>
@endsection

@include('motoyabooks.components.sidebar')
