@extends('motoyabooks.components.user_common')

@section('id', 'mypage')

@include('motoyabooks.components.header')

@section('content')
  <section class="user-menu">
    <ul class="user-menu__list">
      <li class="user-menu__list_element">
        <a href="#">プロフィール</a>
      </li>
      <li class="user-menu__list_element">
        <a href="#">注文履歴の確認</a>
      </li>
      <li class="user-menu__list_element">
        <a href="#">閲覧履歴の確認・削除</a>
      </li>
      <li class="user-menu__list_element">
        <a href="#">本の確認</a>
      </li>
    </ul>
  </section>
@endsection
