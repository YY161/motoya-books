<header>
  <nav class="global-nav">
    <div class="nav-left">
      <div class="logo">MotoyaBooks</div>
    </div>
    <div class="nav-center">
      <form action="{{ route('search') }}" method="GET" class="search-form">
        <input type="search" value="{{ $keyword ?? '' }}" placeholder="検索" name="keyword" class="search-form__search-box">
        <button type="submit" class="search-form__search-button">
          <img src="{{ asset('img/icon-search.svg') }}" alt="" class="search-form__search-icon">
        </button>
      </form>
    </div>
    <div class="nav-right">
      <ul class="usermenu-list">
        @if (Auth::check())
          <li class="usermenu-list__element">
            <a class="usermenu-list__link" href="{{ route('mypage') }}">マイページ</a>
          </li>
          <li class="usermenu-list__element">
            <form method="POST" action="{{ route('logout') }}">
              @csrf
              <a class="usermenu-list__link" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                  this.closest('form').submit();">
                ログアウト
              </a>
            </form>
          </li>
        @else
          <li class="usermenu-list__element">
            <a class="usermenu-list__link" href="{{ route('login') }}">ログイン</a>
          </li>
          <li class="usermenu-list__element">
            <a class="usermenu-list__link" href="{{ route('register') }}">会員登録</a>
          </li>
        @endif
      </ul>
    </div>
  </nav>
</header>
