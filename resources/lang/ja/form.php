<?php

return [

    'Email' => 'メールアドレス',
    'Password' => 'パスワード',
    'Confirm Password' => 'パスワード確認',
    'Remember me' => 'ログイン情報を保存する',
    'Forgot your password?' => 'パスワードをお忘れですか？',
    'Log in' => 'ログイン',
    'Log out' => 'ログアウト',
    'Name' => 'ユーザー名',
    'Confirm' => '確認',
    'Already registered?' => '登録済みですか？',
    'Register' => '登録',
    'Reset Password' => 'パスワードをリセット',
    'send password reset link' => 'メールアドレスを入力してください。入力されたメールアドレスにパスワードリセット用のリンクをお送りします。',
    'Email Password Reset Link' => 'パスワードリセット用リンクを送る',
    'Please confirm password' => '確認のため、パスワードを入力してください。',
    'Resend Verification Email' => '確認用メールを再送信する',

];
