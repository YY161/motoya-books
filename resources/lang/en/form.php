<?php

return [

    'failed' => 'These credentials do not match our records.',
    'send password reset link' => 'Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.',
    'Please confirm password' => 'This is a secure area of the application. Please confirm your password before continuing.',
];
