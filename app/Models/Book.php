<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Book extends Model
{
    use HasFactory;

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function getShortTitleAttribute()
    {
        return Str::limit($this->title, 40);
    }

    public function getPriceAttribute($value)
    {
        return number_format($value);
    }

    public function getPublishedAtAttribute($value)
    {
        return Carbon::parse($value)->format("Y/m/d");
    }

    public function getReviewsCountAttribute()
    {
        return $this->reviews->count();
    }

    public function getReviewsAverageAttribute()
    {
        $review_average = $this->reviews->avg('score');
        $rounded_score = floor($review_average * 2) / 2;
        return $rounded_score;
    }

    public function getRecentReviewsAttribute()
    {
        return $this->reviews->take(3);
    }

    public function getOtherReviewsAttribute()
    {
        return $this->reviews->skip(3);
    }
}
