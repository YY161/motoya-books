<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Review;

class MainController extends Controller
{
    public function index()
    {
        $books = Book::with('reviews')->orderByDesc('published_at', 'desc')->get();

        return view('motoyabooks.index', compact('books'));
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
        $query = Book::with('reviews');

        if ($request->filled('keyword')) {
            $query->where('title', 'LIKE', "%{$keyword}%")
                ->orWhere('description', 'LIKE', "%{$keyword}%");
        }

        $books = $query->paginate(30);

        return view('motoyabooks.search', compact('books', 'keyword'));
    }

    public function detail(Request $request, $id)
    {
        $book = Book::findOrFail($id);

        return view('motoyabooks.detail', compact('book'));
    }
}
