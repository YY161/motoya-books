<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\Book;
use \App\Models\Review;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Book::query()->delete();
        Review::query()->delete();

        Book::factory()
            ->count(30)
            ->create()
            ->each(function ($book) {
                Review::factory()
                    ->count(rand(0, 12))
                    ->for($book)
                    ->create();
            });

    }
}
